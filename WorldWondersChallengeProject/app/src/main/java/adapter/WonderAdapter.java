package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ciandt.challengeandroid.worldwondersapp.R;

import java.util.List;

import entities.Wonder;

/**
 * Created by Jonas on 19/07/2015.
 */
public class WonderAdapter extends ArrayAdapter<Wonder> {

    private LayoutInflater inflater;
    private final List<Wonder> itemsWonder;

    public WonderAdapter(Context context, List<Wonder> wonders) {

        super(context, R.layout.list_wonders_item, wonders);

        this.itemsWonder = wonders;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private static class ViewHolder {
        public TextView tvWonderName;
        public TextView tvWonderCountry;
        public TextView tvWonderDesciption;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_wonders_item, parent, false);
            holder = new ViewHolder();

            holder.tvWonderName = (TextView) convertView.findViewById(R.id.tv_wonder_name);
            holder.tvWonderCountry = (TextView) convertView.findViewById(R.id.tv_wonder_country);
            holder.tvWonderDesciption = (TextView) convertView.findViewById(R.id.tv_wonder_description);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvWonderName.setText(itemsWonder.get(position).getName());
        holder.tvWonderCountry.setText(itemsWonder.get(position).getCountry());
        holder.tvWonderDesciption.setText(itemsWonder.get(position).getDescription());

        return convertView;
    }

}