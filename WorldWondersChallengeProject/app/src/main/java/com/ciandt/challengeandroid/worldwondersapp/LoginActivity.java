package com.ciandt.challengeandroid.worldwondersapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import entities.LoginEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class LoginActivity extends Activity {

    private EditText etEmail, etSenha;
    private Button btnEntrar;

    private String email, senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(MainActivity.isLogged){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        etEmail = (EditText) findViewById(R.id.et_email);
        etSenha = (EditText) findViewById(R.id.et_senha);
        btnEntrar = (Button) findViewById(R.id.btn_entrar);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                 * Validations
                 * Email and Senha can not be empty
                 */
                if(etEmail.getText().length() == 0){
                    etEmail.setError(getString(R.string.login_activity_campo_obrigatorio));
                }else if(etSenha.getText().length() == 0){
                    etSenha.setError(getString(R.string.login_activity_campo_obrigatorio));
                }else{
                    //goNextActivity
                    //http://private-anon-c0c6a4439-worldwondersapi.apiary-mock.com
                    email = etEmail.getText().toString();
                    senha = etSenha.getText().toString();

                    CallLoginURL callLoginURL = new CallLoginURL();
                    callLoginURL.execute("http://private-anon-c0c6a4439-worldwondersapi.apiary-mock.com/api/v1/login?email=" + email + "&senha=" + senha);
                }
            }
        });

    }

    private class CallLoginURL extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];

            String result = "";

            try {

                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line);
                }

                result = sb.toString();

            } catch (Exception e ) {
                System.out.println(e.getMessage());
                return e.getMessage();
            }

            return result;
        }

        protected void onPostExecute(String result) {

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();

            LoginEntity loginEntity = null;

            //Converting the string contents to the jsonEntity
            try {
                loginEntity = gson.fromJson(result, LoginEntity.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }

            if(loginEntity != null){

                if(loginEntity.codigo == 1){
                    //Invalid credentials, show Toast
                    Toast.makeText(getApplicationContext(), loginEntity.mensagem, Toast.LENGTH_SHORT).show();
                }else if(loginEntity.codigo == 0){
                    //Success
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("NOME", loginEntity.dados.nome);
                    startActivity(intent);
                }

            }
        }
    }
}
