package com.ciandt.challengeandroid.worldwondersapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import adapter.WonderAdapter;
import entities.WondersEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends ActionBarActivity {

    private ListView lvWonders;
    private WonderAdapter wonderAdapter;

    /*
     * Used a static variable to control login,
     * in case of android app memory clear,
     * is necessary to login again.
     * Could be used a SharedPreference but
     * would not be necessary to login again in
     * state case above.
     */
    public static boolean isLogged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isLogged = true;

        String nome = getIntent().getStringExtra("NOME");

        if(nome != null){
            Toast.makeText(this, getString(R.string.main_activity_ola) + nome, Toast.LENGTH_LONG).show();
        }

        lvWonders = (ListView) findViewById(R.id.lv_wonders);

        CallWondersURL callWondersURL = new CallWondersURL();
        callWondersURL.execute("http://private-anon-c0c6a4439-worldwondersapi.apiary-mock.com/api/v1/worldwonders");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_desconectar) {
            isLogged = false;
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);

            // Finish this activity to not be possible to came back by onBackPressed()
            // Also could be used on AndroidManifest the noHistory attribute for activity as was done for LoginActivity
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class CallWondersURL extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];

            String result = "";

            try {

                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line);
                }

                result = sb.toString();

            } catch (Exception e ) {
                System.out.println(e.getMessage());
                return e.getMessage();
            }

            return result;
        }

        protected void onPostExecute(String result) {

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();

            WondersEntity wondersEntity = null;

            //Converting the string contents to the jsonEntity
            try {
                wondersEntity = gson.fromJson(result, WondersEntity.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }

            if(wondersEntity != null){
                wonderAdapter = new WonderAdapter(getApplicationContext(), wondersEntity.data);
                lvWonders.setAdapter(wonderAdapter);
            }
        }
    }
}
